package entityDB;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "LIEU")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lieu.findAll", query = "SELECT l FROM Lieu l"),
    @NamedQuery(name = "Lieu.findById", query = "SELECT l FROM Lieu l WHERE l.id = :id"),
    @NamedQuery(name = "Lieu.findByNom", query = "SELECT l FROM Lieu l WHERE l.nom = :nom"),
    @NamedQuery(name = "Lieu.findBySuppressionLogique", query = "SELECT l FROM Lieu l WHERE l.suppressionLogique = :suppressionLogique"),
    @NamedQuery(name = "Lieu.findByUrlPlan", query = "SELECT l FROM Lieu l WHERE l.urlPlan = :urlPlan")})
public class Lieu implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigDecimal id;
    @Size(max = 255)
    @Column(name = "Nom")
    private String nom;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @Size(max = 255)
    @Column(name = "UrlPlan")
    private String urlPlan;
    @ManyToMany(mappedBy = "lieuCollection")
    private Collection<Evenement> evenementCollection;
    @ManyToMany(mappedBy = "lieuCollection")
    private Collection<Adresse> adresseCollection;
    @OneToMany(mappedBy = "lieuId")
    private Collection<Seance> seanceCollection;
    @OneToMany(mappedBy = "lieuId")
    private Collection<Place> placeCollection;

    public Lieu() {
    }

    public Lieu(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        if(suppressionLogique!=null){
            this.suppressionLogique = suppressionLogique;
        }else{
        this.suppressionLogique = false;
        }
    }

    public String getUrlPlan() {
        return urlPlan;
    }

    public void setUrlPlan(String urlPlan) {
        this.urlPlan = urlPlan;
    }

    @XmlTransient
    public Collection<Evenement> getEvenementCollection() {
        return evenementCollection;
    }

    public void setEvenementCollection(Collection<Evenement> evenementCollection) {
        this.evenementCollection = evenementCollection;
    }

    @XmlTransient
    public Collection<Adresse> getAdresseCollection() {
        return adresseCollection;
    }

    public void setAdresseCollection(Collection<Adresse> adresseCollection) {
        this.adresseCollection = adresseCollection;
    }

    @XmlTransient
    public Collection<Seance> getSeanceCollection() {
        return seanceCollection;
    }

    public void setSeanceCollection(Collection<Seance> seanceCollection) {
        this.seanceCollection = seanceCollection;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(Collection<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lieu)) {
            return false;
        }
        Lieu other = (Lieu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entityDB.Lieu[ id=" + id + " ]";
    }
    
}
