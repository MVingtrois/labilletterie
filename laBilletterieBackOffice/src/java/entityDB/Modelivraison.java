package entityDB;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "MODELIVRAISON")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modelivraison.findAll", query = "SELECT m FROM Modelivraison m"),
    @NamedQuery(name = "Modelivraison.findById", query = "SELECT m FROM Modelivraison m WHERE m.id = :id"),
    @NamedQuery(name = "Modelivraison.findBySuppressionLogique", query = "SELECT m FROM Modelivraison m WHERE m.suppressionLogique = :suppressionLogique"),
    @NamedQuery(name = "Modelivraison.findByType", query = "SELECT m FROM Modelivraison m WHERE m.type = :type")})
public class Modelivraison implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigDecimal id;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @Size(max = 255)
    @Column(name = "Type")
    private String type;
    @OneToMany(mappedBy = "modelivraisonId")
    private Collection<Commande> commandeCollection;

    public Modelivraison() {
    }

    public Modelivraison(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        if(suppressionLogique!=null){
            this.suppressionLogique = suppressionLogique;
        }else{
        this.suppressionLogique = false;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlTransient
    public Collection<Commande> getCommandeCollection() {
        return commandeCollection;
    }

    public void setCommandeCollection(Collection<Commande> commandeCollection) {
        this.commandeCollection = commandeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modelivraison)) {
            return false;
        }
        Modelivraison other = (Modelivraison) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entityDB.Modelivraison[ id=" + id + " ]";
    }
    
}
