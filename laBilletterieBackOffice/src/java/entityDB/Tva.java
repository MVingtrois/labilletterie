package entityDB;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@Entity
@Table(name = "TVA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tva.findAll", query = "SELECT t FROM Tva t"),
    @NamedQuery(name = "Tva.findById", query = "SELECT t FROM Tva t WHERE t.id = :id"),
    @NamedQuery(name = "Tva.findByDate", query = "SELECT t FROM Tva t WHERE t.date = :date"),
    @NamedQuery(name = "Tva.findBySuppressionLogique", query = "SELECT t FROM Tva t WHERE t.suppressionLogique = :suppressionLogique"),
    @NamedQuery(name = "Tva.findByTaux", query = "SELECT t FROM Tva t WHERE t.taux = :taux"),
    @NamedQuery(name = "Tva.findByType", query = "SELECT t FROM Tva t WHERE t.type = :type")})
public class Tva implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigDecimal id;
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @Column(name = "Taux")
    private Float taux;
    @Size(max = 255)
    @Column(name = "Type")
    private String type;
    @OneToMany(mappedBy = "tvaId")
    private Collection<Evenement> evenementCollection;

    public Tva() {
    }

    public Tva(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        if(suppressionLogique!=null){
            this.suppressionLogique = suppressionLogique;
        }else{
        this.suppressionLogique = false;
        }
    }

    public Float getTaux() {
        return taux;
    }

    public void setTaux(Float taux) {
        this.taux = taux;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlTransient
    public Collection<Evenement> getEvenementCollection() {
        return evenementCollection;
    }

    public void setEvenementCollection(Collection<Evenement> evenementCollection) {
        this.evenementCollection = evenementCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tva)) {
            return false;
        }
        Tva other = (Tva) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entityDB.Tva[ id=" + id + " ]";
    }
    
}
