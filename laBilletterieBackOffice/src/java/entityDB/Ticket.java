package entityDB;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "TICKET")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ticket.findAll", query = "SELECT t FROM Ticket t"),
    @NamedQuery(name = "Ticket.findById", query = "SELECT t FROM Ticket t WHERE t.id = :id"),
    @NamedQuery(name = "Ticket.findByETicket", query = "SELECT t FROM Ticket t WHERE t.eTicket = :eTicket"),
    @NamedQuery(name = "Ticket.findByNumero", query = "SELECT t FROM Ticket t WHERE t.numero = :numero"),
    @NamedQuery(name = "Ticket.findBySuppressionLogique", query = "SELECT t FROM Ticket t WHERE t.suppressionLogique = :suppressionLogique")})
public class Ticket implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigDecimal id;
    @Column(name = "ETicket")
    private Boolean eTicket;
    @Size(max = 255)
    @Column(name = "Numero")
    private String numero;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @JoinColumn(name = "SEANCE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Seance seanceId;
    @JoinColumn(name = "PLACE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Place placeId;
    @JoinColumn(name = "COMMANDE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Commande commandeId;

    public Ticket() {
    }

    public Ticket(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Boolean getETicket() {
        return eTicket;
    }

    public void setETicket(Boolean eTicket) {
        this.eTicket = eTicket;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        if(suppressionLogique!=null){
            this.suppressionLogique = suppressionLogique;
        }else{
        this.suppressionLogique = false;
        }
    }

    public Seance getSeanceId() {
        return seanceId;
    }

    public void setSeanceId(Seance seanceId) {
        this.seanceId = seanceId;
    }

    public Place getPlaceId() {
        return placeId;
    }

    public void setPlaceId(Place placeId) {
        this.placeId = placeId;
    }

    public Commande getCommandeId() {
        return commandeId;
    }

    public void setCommandeId(Commande commandeId) {
        this.commandeId = commandeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ticket)) {
            return false;
        }
        Ticket other = (Ticket) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entityDB.Ticket[ id=" + id + " ]";
    }
    
}
