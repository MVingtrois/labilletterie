/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sessionejb;

import entite.Adresse;
import entite.Client;
import javax.ejb.Local;

/**
 *
 * @author cdi206
 */
@Local
public interface InscriptionLocal {
     
      
      public void persist(Object objet);

    public void Inscrire(Client client, Adresse adresse);
}
