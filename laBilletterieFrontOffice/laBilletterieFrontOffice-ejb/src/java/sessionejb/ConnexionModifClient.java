/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionejb;

import entite.Client;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author cdi206
 */
@Stateful
public class ConnexionModifClient implements ConnexionModifClientLocal, Serializable {

    @PersistenceContext(unitName = "laBilletterieFrontOffice-ejbPU")
    private EntityManager em;

    @Override
    public Client connexion(String login, String password) {
        List<Client> listClients = new ArrayList<>();
        Query qr = em.createQuery("SELECT c FROM Client c WHERE c.login=:login AND c.motDePasse=:password");
        qr.setParameter("login", login);
        qr.setParameter("password", password);
        listClients = qr.getResultList();

        Client client = new Client();
        client.setId(BigDecimal.valueOf(500));
        client.setNom("Erreur");

        if (listClients.size() > 0) {
            client = listClients.get(0);
        }

        System.out.println("-------------listclient : " + listClients);
        return client;
    }

    public void modifClient(Client client) {
        Query qr = em.createQuery("Update Client c set c.civilite=:civilite, "
                + "c.login=:login, "
                + "c.mail=:mail, "
                + "c.motDePasse=:motDePasse, "
                + "c.naissance=:naissance, "
                + "c.nom=:nom, "
                + "c.prenom=:prenom "
                + "WHERE c.id=:id");
        
        qr.setParameter("civilite", client.getCivilite());
        qr.setParameter("login", client.getLogin());
        qr.setParameter("motDePasse", client.getMotDePasse());
        qr.setParameter("mail", client.getMail());
        qr.setParameter("naissance", client.getNaissance());
        qr.setParameter("nom", client.getNom());
        qr.setParameter("prenom", client.getPrenom());
        qr.setParameter("id", client.getId());
        qr.executeUpdate();
        

    }

    public void persist(Object object) {
        em.persist(object);
    }

}
