/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionejb;

import entite.Client;
import javax.ejb.CreateException;
import javax.ejb.Local;

/**
 *
 * @author cdi206
 */
@Local
public interface ConnexionModifClientLocal {

    public Client connexion(String login, String password);

    public void modifClient(Client client);

}
