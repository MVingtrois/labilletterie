/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sessionejb;

import entite.Adresse;
import entite.Client;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author cdi206
 */
@Stateless
public class Inscription implements InscriptionLocal {

    @PersistenceContext(unitName = "laBilletterieFrontOffice-ejbPU")
    private EntityManager em;

    @Override
    public void Inscrire(Client client, Adresse adresse) {
        System.out.println("(EJB session Inscription client : "+client.toString());
        System.out.println("(EJB session Inscription adresse : "+adresse.toString());
        List<Adresse> listAdresse = new ArrayList();
        client.setAdresseCollection(listAdresse);
        client.getAdresseCollection().add(adresse);
        
        System.out.println("EJB : " + adresse.toString() + client.toString());

        persist(client);

    }

    @Override
    public void persist(Object object) {
        em.persist(object);
    }

}
