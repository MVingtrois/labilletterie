package sessionejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class ListEvenement implements ListEvenementLocal {
    @PersistenceContext(unitName = "laBilletterieFrontOffice-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }



}
