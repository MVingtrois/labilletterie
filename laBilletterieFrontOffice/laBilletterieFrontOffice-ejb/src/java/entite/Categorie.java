/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "CATEGORIE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categorie.findAll", query = "SELECT c FROM Categorie c"),
    @NamedQuery(name = "Categorie.findById", query = "SELECT c FROM Categorie c WHERE c.id = :id"),
    @NamedQuery(name = "Categorie.findByLibelle", query = "SELECT c FROM Categorie c WHERE c.libelle = :libelle"),
    @NamedQuery(name = "Categorie.findByPrix", query = "SELECT c FROM Categorie c WHERE c.prix = :prix"),
    @NamedQuery(name = "Categorie.findBySuppressionLogique", query = "SELECT c FROM Categorie c WHERE c.suppressionLogique = :suppressionLogique")})
public class Categorie implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Size(max = 255)
    @Column(name = "Libelle")
    private String libelle;
    @Column(name = "Prix")
    private Float prix;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @JoinTable(name = "REDUCTION_CATEGORIE", joinColumns = {
        @JoinColumn(name = "lesCategories_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "lesReductions_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Reduction> reductionCollection;
    @OneToMany(mappedBy = "categorieId")
    private Collection<Place> placeCollection;

    public Categorie() {
    }

    public Categorie(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    @XmlTransient
    public Collection<Reduction> getReductionCollection() {
        return reductionCollection;
    }

    public void setReductionCollection(Collection<Reduction> reductionCollection) {
        this.reductionCollection = reductionCollection;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(Collection<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categorie)) {
            return false;
        }
        Categorie other = (Categorie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entite.Categorie[ id=" + id + " ]";
    }
    
}
