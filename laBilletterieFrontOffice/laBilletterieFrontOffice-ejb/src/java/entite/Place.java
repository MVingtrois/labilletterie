/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "PLACE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Place.findAll", query = "SELECT p FROM Place p"),
    @NamedQuery(name = "Place.findById", query = "SELECT p FROM Place p WHERE p.id = :id"),
    @NamedQuery(name = "Place.findByDisponible", query = "SELECT p FROM Place p WHERE p.disponible = :disponible"),
    @NamedQuery(name = "Place.findByNumero", query = "SELECT p FROM Place p WHERE p.numero = :numero"),
    @NamedQuery(name = "Place.findBySuppressionLogique", query = "SELECT p FROM Place p WHERE p.suppressionLogique = :suppressionLogique")})
public class Place implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Disponible")
    private boolean disponible;
    @Size(max = 255)
    @Column(name = "Numero")
    private String numero;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @JoinColumn(name = "ZONE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Zone zoneId;
    @OneToMany(mappedBy = "placeprecedeId")
    private Collection<Place> placeCollection;
    @JoinColumn(name = "PLACEPRECEDE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Place placeprecedeId;
    @OneToMany(mappedBy = "placesuiviId")
    private Collection<Place> placeCollection1;
    @JoinColumn(name = "PLACESUIVI_ID", referencedColumnName = "ID")
    @ManyToOne
    private Place placesuiviId;
    @JoinColumn(name = "LIEU_ID", referencedColumnName = "ID")
    @ManyToOne
    private Lieu lieuId;
    @JoinColumn(name = "CATEGORIE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Categorie categorieId;
    @OneToMany(mappedBy = "placeId")
    private Collection<Ticket> ticketCollection;

    public Place() {
    }

    public Place(BigDecimal id) {
        this.id = id;
    }

    public Place(BigDecimal id, boolean disponible) {
        this.id = id;
        this.disponible = disponible;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public boolean getDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Zone getZoneId() {
        return zoneId;
    }

    public void setZoneId(Zone zoneId) {
        this.zoneId = zoneId;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection() {
        return placeCollection;
    }

    public void setPlaceCollection(Collection<Place> placeCollection) {
        this.placeCollection = placeCollection;
    }

    public Place getPlaceprecedeId() {
        return placeprecedeId;
    }

    public void setPlaceprecedeId(Place placeprecedeId) {
        this.placeprecedeId = placeprecedeId;
    }

    @XmlTransient
    public Collection<Place> getPlaceCollection1() {
        return placeCollection1;
    }

    public void setPlaceCollection1(Collection<Place> placeCollection1) {
        this.placeCollection1 = placeCollection1;
    }

    public Place getPlacesuiviId() {
        return placesuiviId;
    }

    public void setPlacesuiviId(Place placesuiviId) {
        this.placesuiviId = placesuiviId;
    }

    public Lieu getLieuId() {
        return lieuId;
    }

    public void setLieuId(Lieu lieuId) {
        this.lieuId = lieuId;
    }

    public Categorie getCategorieId() {
        return categorieId;
    }

    public void setCategorieId(Categorie categorieId) {
        this.categorieId = categorieId;
    }

    @XmlTransient
    public Collection<Ticket> getTicketCollection() {
        return ticketCollection;
    }

    public void setTicketCollection(Collection<Ticket> ticketCollection) {
        this.ticketCollection = ticketCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Place)) {
            return false;
        }
        Place other = (Place) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entite.Place[ id=" + id + " ]";
    }
    
}
