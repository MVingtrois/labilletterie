/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "REDUCTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reduction.findAll", query = "SELECT r FROM Reduction r"),
    @NamedQuery(name = "Reduction.findById", query = "SELECT r FROM Reduction r WHERE r.id = :id"),
    @NamedQuery(name = "Reduction.findByLibelle", query = "SELECT r FROM Reduction r WHERE r.libelle = :libelle"),
    @NamedQuery(name = "Reduction.findByMontant", query = "SELECT r FROM Reduction r WHERE r.montant = :montant"),
    @NamedQuery(name = "Reduction.findBySuppressionLogique", query = "SELECT r FROM Reduction r WHERE r.suppressionLogique = :suppressionLogique")})
public class Reduction implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Size(max = 255)
    @Column(name = "Libelle")
    private String libelle;
    @Column(name = "Montant")
    private Float montant;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @ManyToMany(mappedBy = "reductionCollection")
    private Collection<Categorie> categorieCollection;

    public Reduction() {
    }

    public Reduction(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getMontant() {
        return montant;
    }

    public void setMontant(Float montant) {
        this.montant = montant;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    @XmlTransient
    public Collection<Categorie> getCategorieCollection() {
        return categorieCollection;
    }

    public void setCategorieCollection(Collection<Categorie> categorieCollection) {
        this.categorieCollection = categorieCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reduction)) {
            return false;
        }
        Reduction other = (Reduction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entite.Reduction[ id=" + id + " ]";
    }
    
}
