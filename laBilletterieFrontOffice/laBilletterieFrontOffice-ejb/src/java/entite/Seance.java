/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "SEANCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seance.findAll", query = "SELECT s FROM Seance s"),
    @NamedQuery(name = "Seance.findById", query = "SELECT s FROM Seance s WHERE s.id = :id"),
    @NamedQuery(name = "Seance.findByDate", query = "SELECT s FROM Seance s WHERE s.date = :date"),
    @NamedQuery(name = "Seance.findByDuree", query = "SELECT s FROM Seance s WHERE s.duree = :duree"),
    @NamedQuery(name = "Seance.findByHeure", query = "SELECT s FROM Seance s WHERE s.heure = :heure"),
    @NamedQuery(name = "Seance.findByLibelle", query = "SELECT s FROM Seance s WHERE s.libelle = :libelle"),
    @NamedQuery(name = "Seance.findBySuppressionLogique", query = "SELECT s FROM Seance s WHERE s.suppressionLogique = :suppressionLogique")})
public class Seance implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "Date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(max = 255)
    @Column(name = "Duree")
    private String duree;
    @Column(name = "Heure")
    @Temporal(TemporalType.TIMESTAMP)
    private Date heure;
    @Size(max = 255)
    @Column(name = "Libelle")
    private String libelle;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @JoinColumn(name = "LIEU_ID", referencedColumnName = "ID")
    @ManyToOne
    private Lieu lieuId;
    @JoinColumn(name = "EVENEMENT_ID", referencedColumnName = "ID")
    @ManyToOne
    private Evenement evenementId;
    @OneToMany(mappedBy = "seanceId")
    private Collection<Ticket> ticketCollection;

    public Seance() {
    }

    public Seance(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public Date getHeure() {
        return heure;
    }

    public void setHeure(Date heure) {
        this.heure = heure;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Lieu getLieuId() {
        return lieuId;
    }

    public void setLieuId(Lieu lieuId) {
        this.lieuId = lieuId;
    }

    public Evenement getEvenementId() {
        return evenementId;
    }

    public void setEvenementId(Evenement evenementId) {
        this.evenementId = evenementId;
    }

    @XmlTransient
    public Collection<Ticket> getTicketCollection() {
        return ticketCollection;
    }

    public void setTicketCollection(Collection<Ticket> ticketCollection) {
        this.ticketCollection = ticketCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seance)) {
            return false;
        }
        Seance other = (Seance) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entite.Seance[ id=" + id + " ]";
    }
    
}
