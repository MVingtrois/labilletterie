/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "COMMANDE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Commande.findAll", query = "SELECT c FROM Commande c"),
    @NamedQuery(name = "Commande.findById", query = "SELECT c FROM Commande c WHERE c.id = :id"),
    @NamedQuery(name = "Commande.findByDateCommande", query = "SELECT c FROM Commande c WHERE c.dateCommande = :dateCommande"),
    @NamedQuery(name = "Commande.findByMontant", query = "SELECT c FROM Commande c WHERE c.montant = :montant"),
    @NamedQuery(name = "Commande.findByNumeroCommande", query = "SELECT c FROM Commande c WHERE c.numeroCommande = :numeroCommande"),
    @NamedQuery(name = "Commande.findByStatut", query = "SELECT c FROM Commande c WHERE c.statut = :statut"),
    @NamedQuery(name = "Commande.findBySuppressionLogique", query = "SELECT c FROM Commande c WHERE c.suppressionLogique = :suppressionLogique")})
public class Commande implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Column(name = "DateCommande")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCommande;
    @Column(name = "Montant")
    private BigInteger montant;
    @Size(max = 255)
    @Column(name = "NumeroCommande")
    private String numeroCommande;
    @Size(max = 255)
    @Column(name = "Statut")
    private String statut;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @OneToMany(mappedBy = "commandeId")
    private Collection<Ticket> ticketCollection;
    @JoinColumn(name = "MODEPAIEMENT_ID", referencedColumnName = "ID")
    @ManyToOne
    private Modepaiment modepaiementId;
    @JoinColumn(name = "MODELIVRAISON_ID", referencedColumnName = "ID")
    @ManyToOne
    private Modelivraison modelivraisonId;
    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID")
    @ManyToOne
    private Client clientId;
    @JoinColumn(name = "ADRESSELIVRAISON_ID", referencedColumnName = "ID")
    @ManyToOne
    private Adresse adresselivraisonId;
    @OneToMany(mappedBy = "facturecommandeId")
    private Collection<Adresse> adresseCollection;

    public Commande() {
    }

    public Commande(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public BigInteger getMontant() {
        return montant;
    }

    public void setMontant(BigInteger montant) {
        this.montant = montant;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    @XmlTransient
    public Collection<Ticket> getTicketCollection() {
        return ticketCollection;
    }

    public void setTicketCollection(Collection<Ticket> ticketCollection) {
        this.ticketCollection = ticketCollection;
    }

    public Modepaiment getModepaiementId() {
        return modepaiementId;
    }

    public void setModepaiementId(Modepaiment modepaiementId) {
        this.modepaiementId = modepaiementId;
    }

    public Modelivraison getModelivraisonId() {
        return modelivraisonId;
    }

    public void setModelivraisonId(Modelivraison modelivraisonId) {
        this.modelivraisonId = modelivraisonId;
    }

    public Client getClientId() {
        return clientId;
    }

    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }

    public Adresse getAdresselivraisonId() {
        return adresselivraisonId;
    }

    public void setAdresselivraisonId(Adresse adresselivraisonId) {
        this.adresselivraisonId = adresselivraisonId;
    }

    @XmlTransient
    public Collection<Adresse> getAdresseCollection() {
        return adresseCollection;
    }

    public void setAdresseCollection(Collection<Adresse> adresseCollection) {
        this.adresseCollection = adresseCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entite.Commande[ id=" + id + " ]";
    }
    
}
