/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "EVENEMENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evenement.findAll", query = "SELECT e FROM Evenement e"),
    @NamedQuery(name = "Evenement.findById", query = "SELECT e FROM Evenement e WHERE e.id = :id"),
    @NamedQuery(name = "Evenement.findByDescription", query = "SELECT e FROM Evenement e WHERE e.description = :description"),
    @NamedQuery(name = "Evenement.findBySuppressionLogique", query = "SELECT e FROM Evenement e WHERE e.suppressionLogique = :suppressionLogique"),
    @NamedQuery(name = "Evenement.findByTitre", query = "SELECT e FROM Evenement e WHERE e.titre = :titre"),
    @NamedQuery(name = "Evenement.findByUrlAffiche", query = "SELECT e FROM Evenement e WHERE e.urlAffiche = :urlAffiche")})
public class Evenement implements Serializable {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Size(max = 255)
    @Column(name = "Description")
    private String description;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @Size(max = 255)
    @Column(name = "Titre")
    private String titre;
    @Size(max = 255)
    @Column(name = "UrlAffiche")
    private String urlAffiche;
    @JoinTable(name = "PARTICIPANT_EVENEMENT", joinColumns = {
        @JoinColumn(name = "lesEvenements_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "lesParticipants_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Participant> participantCollection;
    @JoinTable(name = "LIEU_EVENEMENT", joinColumns = {
        @JoinColumn(name = "lesEvenements_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "lesLieux_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Lieu> lieuCollection;
    @JoinColumn(name = "TVA_ID", referencedColumnName = "ID")
    @ManyToOne
    private Tva tvaId;
    @JoinColumn(name = "THEME_ID", referencedColumnName = "ID")
    @ManyToOne
    private Theme themeId;
    @JoinColumn(name = "GENRE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Genre genreId;
    @OneToMany(mappedBy = "evenementId")
    private Collection<Seance> seanceCollection;

    public Evenement() {
    }

    public Evenement(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUrlAffiche() {
        return urlAffiche;
    }

    public void setUrlAffiche(String urlAffiche) {
        this.urlAffiche = urlAffiche;
    }

    @XmlTransient
    public Collection<Participant> getParticipantCollection() {
        return participantCollection;
    }

    public void setParticipantCollection(Collection<Participant> participantCollection) {
        this.participantCollection = participantCollection;
    }

    @XmlTransient
    public Collection<Lieu> getLieuCollection() {
        return lieuCollection;
    }

    public void setLieuCollection(Collection<Lieu> lieuCollection) {
        this.lieuCollection = lieuCollection;
    }

    public Tva getTvaId() {
        return tvaId;
    }

    public void setTvaId(Tva tvaId) {
        this.tvaId = tvaId;
    }

    public Theme getThemeId() {
        return themeId;
    }

    public void setThemeId(Theme themeId) {
        this.themeId = themeId;
    }

    public Genre getGenreId() {
        return genreId;
    }

    public void setGenreId(Genre genreId) {
        this.genreId = genreId;
    }

    @XmlTransient
    public Collection<Seance> getSeanceCollection() {
        return seanceCollection;
    }

    public void setSeanceCollection(Collection<Seance> seanceCollection) {
        this.seanceCollection = seanceCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evenement)) {
            return false;
        }
        Evenement other = (Evenement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entite.Evenement[ id=" + id + " ]";
    }
    
}
