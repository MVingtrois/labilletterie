/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entite;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author cdi206
 */
@Entity
@Table(name = "ADRESSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Adresse.findAll", query = "SELECT a FROM Adresse a"),
    @NamedQuery(name = "Adresse.findById", query = "SELECT a FROM Adresse a WHERE a.id = :id"),
    @NamedQuery(name = "Adresse.findByCodePostal", query = "SELECT a FROM Adresse a WHERE a.codePostal = :codePostal"),
    @NamedQuery(name = "Adresse.findByNomRue", query = "SELECT a FROM Adresse a WHERE a.nomRue = :nomRue"),
    @NamedQuery(name = "Adresse.findByNumeroRue", query = "SELECT a FROM Adresse a WHERE a.numeroRue = :numeroRue"),
    @NamedQuery(name = "Adresse.findBySuppressionLogique", query = "SELECT a FROM Adresse a WHERE a.suppressionLogique = :suppressionLogique"),
    @NamedQuery(name = "Adresse.findByTypeRue", query = "SELECT a FROM Adresse a WHERE a.typeRue = :typeRue"),
    @NamedQuery(name = "Adresse.findByVille", query = "SELECT a FROM Adresse a WHERE a.ville = :ville")})
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigDecimal id;
    @Size(max = 255)
    @Column(name = "CodePostal")
    private String codePostal;
    @Size(max = 255)
    @Column(name = "NomRue")
    private String nomRue;
    @Column(name = "NumeroRue")
    private Integer numeroRue;
    @Column(name = "SuppressionLogique")
    private Boolean suppressionLogique;
    @Size(max = 255)
    @Column(name = "TypeRue")
    private String typeRue;
    @Size(max = 255)
    @Column(name = "Ville")
    private String ville;
    @JoinTable(name = "ADRESSE_LIEU", joinColumns = {
        @JoinColumn(name = "lesAdresses_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "lesLieux_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Lieu> lieuCollection;
    @JoinTable(name = "ADRESSE_CLIENT", joinColumns = {
        @JoinColumn(name = "lesAdresses_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
        @JoinColumn(name = "lesClients_ID", referencedColumnName = "ID")})
    @ManyToMany
    private Collection<Client> clientCollection;
    @OneToMany(mappedBy = "adresselivraisonId")
    private Collection<Commande> commandeCollection;
    @JoinColumn(name = "FACTURECOMMANDE_ID", referencedColumnName = "ID")
    @ManyToOne
    private Commande facturecommandeId;

    public Adresse() {
    }

    public Adresse(BigDecimal id) {
        this.id = id;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public Integer getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(Integer numeroRue) {
        this.numeroRue = numeroRue;
    }

    public Boolean getSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(Boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public String getTypeRue() {
        return typeRue;
    }

    public void setTypeRue(String typeRue) {
        this.typeRue = typeRue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @XmlTransient
    public Collection<Lieu> getLieuCollection() {
        return lieuCollection;
    }

    public void setLieuCollection(Collection<Lieu> lieuCollection) {
        this.lieuCollection = lieuCollection;
    }

    @XmlTransient
    public Collection<Client> getClientCollection() {
        return clientCollection;
    }

    public void setClientCollection(Collection<Client> clientCollection) {
        this.clientCollection = clientCollection;
    }

    @XmlTransient
    public Collection<Commande> getCommandeCollection() {
        return commandeCollection;
    }

    public void setCommandeCollection(Collection<Commande> commandeCollection) {
        this.commandeCollection = commandeCollection;
    }

    public Commande getFacturecommandeId() {
        return facturecommandeId;
    }

    public void setFacturecommandeId(Commande facturecommandeId) {
        this.facturecommandeId = facturecommandeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adresse)) {
            return false;
        }
        Adresse other = (Adresse) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Adresse{" + "id=" + id + ", codePostal=" + codePostal + ", nomRue=" + nomRue + ", numeroRue=" + numeroRue + ", suppressionLogique=" + suppressionLogique + ", typeRue=" + typeRue + ", ville=" + ville + ", lieuCollection=" + lieuCollection + ", clientCollection=" + clientCollection + ", commandeCollection=" + commandeCollection + ", facturecommandeId=" + facturecommandeId + '}';
    }

   

}
