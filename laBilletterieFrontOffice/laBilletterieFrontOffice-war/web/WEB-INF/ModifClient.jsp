<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page Modification</title>
    </head>
    <body>
        
        <form  method="POST" action="Controller?section=SousControllerModifClient">
            <hr>
            <label for="m"><input type="radio" id="m" name="civilite" value="M" checked="checked" />M</label>
            <label for="mme"><input type="radio" id="mme" name="civilite" value="Mme" />Mme</label><br>
            <label for="nom">Nom:</label><input type="text" id="nom" name="nom" value="${nom}" required="true" /><br>
            <label for="prenom">Prenom:</label> <input type="text" id="prenom" name="prenom" value="${prenom}" required="true"/><br>
            <label for="naissance">Date de Naissance(jj/mm/yyyy):</label> <input type="text" id="naissance" name="naissance" value="<fmt:formatDate pattern="dd/MM/yyyy" value="${naissance}" />" required="true"/><br>
            <label for="login">Login:</label> <input type="text" id="login" name="login" value="${login}" /><br>
            <label for="mdp"> Mot de Passe:</label><input type="password" id="mdp" name="mdp" value="${mdp}" required="true" /><br>
            <label for="mail">Email:</label><input type="text" id="mail" name="mail" value="${mail}" required="true" /><br>

            <input type="submit" name="modif" value="Valider" />

            <hr>
        </form>
    </body>
</html>
