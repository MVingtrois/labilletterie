<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link href='<c:url value="http://fonts.googleapis.com/css?family=Open+Sans:400,300|Roboto+Slab:100,400"/>' rel='stylesheet' type='text/css'>
        <link type="text/css" rel="stylesheet" href="<c:url value="CSS/style.css" />" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>La Billetterie | ${ titre }</title>
    </head>
    <body>

        <%-------------------------------------------------
            Header
        --------------------------------------------------%>
        <div class="top">

            <%@include file="header.jsp" %>

            <%@include file="carroussel.jsp" %>

            <div class="spacer"> </div>
        </div>

        <div class="content">
            <%-------------------------------------------------
                Liste Multiple (Rubrique)
            --------------------------------------------------%>
            <c:if test="${multi == 1}">
                <c:forEach var="rubrique" items="${ liste }">
                    <div class="uneRubrique">
                        <h2>${ rubrique.key }</h2>
                        <c:forEach var="livre" items="${ rubrique.value }">
                            <ul class="unLivre">
                                <li class="img tooltip">
                                    <a href="Catalogue?ouvrage=${ livre.value.ouvrage.id }">
                                        <div class="img">
                                            <img class="simple poster" src="img/pti/${ livre.value.isbn }.jpg" title=""  alt="" />
                                            <div class="tooltip">
                                                <span class="titre">${ livre.value.editeur.nom }</span>
                                                <div class="soustitre"><a href="Panier?idEdition=${ livre.value.id }" title="">AJOUTER AU PANIER</a></div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="titre">${ livre.value.ouvrage.titre }</li>
                                <li class="soustitre">
                                    <a class="link" href="Catalogue?auteur=${ livre.value.auteur.id }">${ livre.value.auteur.prenom } ${ livre.value.auteur.nom }</a>
                                </li>
                                <li class="prix">
                                    ${ livre.value.prix.montant } <span class="petit">€</span>
                                    <a href="Panier?idEdition=${ livre.value.id }" title="">
                                        <img class="droite" src="CSS/img/icon_panier-pti.png" title="" alt="" />
                                    </a>
                                </li>
                            </ul>
                        </c:forEach>
                        <div class="unLivre">
                            <a href="Catalogue?rubrique=${ rubrique.key }">
                                <span class="suite">AUTRES<br />${ rubrique.key }
                                </span>
                            </a>
                        </div>
                        <div class="spacer"> </div>
                    </div>
                </c:forEach>
            </c:if>


            <%-------------------------------------------------
                Liste Simple
            --------------------------------------------------%>
            <c:if test="${multi == 0}">
                <div class="uneRubrique">
                    <h2>${ titre }</h2>
                    <c:forEach var="livre" items="${liste}">
                        <ul class="unLivre">
                            <li class="img tooltip">
                                <a href="Catalogue?ouvrage=${ livre.value.ouvrage.id }">
                                    <div class="img">
                                        <img class="simple poster" src="img/pti/${ livre.value.isbn }.jpg" title=""  alt="" />
                                        <div class="tooltip">
                                            <span class="titre">${ livre.value.editeur.nom }</span>
                                            <div class="soustitre"><a href="#">AJOUTER</a></div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="titre">${ livre.value.ouvrage.titre }</li>
                            <li class="soustitre">
                                <a class="link" href="Catalogue?auteur=${ livre.value.auteur.id }">${ livre.value.auteur.prenom } ${ livre.value.auteur.nom }</a>
                            </li>
                            <li class="prix">
                                ${ livre.value.prix.montant } <span class="petit">€</span>
                                <a href="Panier?idEdition=${ livre.value.id }&${ filtre }=${ id }" title="">
                                    <img class="droite" src="CSS/img/icon_panier-pti.png" title="" />
                                </a>
                            </li>
                        </ul>
                    </c:forEach>
                    <div class="spacer"> </div>
                </div>
            </c:if>
        </div>
            
    </body>
</html>
