<div class="carroussel">
    <ul class="lesEvents">
        <c:forEach var="event" items="${lesEvents}">
            <li class="img tooltip">
                <a href="Catalogue?ouvrage=${ livre.value.ouvrage.id }">
                    <div class="img">
                        <img class="simple posterUne" src="img/pti/${ livre.value.isbn }.jpg" title=""  alt="" />
                    </div>
                    <div class="tooltip">
                        <span class="titre">${ livre.value.ouvrage.titre }</span>
                        <span class="soustitre">${ livre.value.ouvrage.soustitre }</span>
                    </div>
                </a>

            </li>
        </c:forEach>
    </ul>
</div>
