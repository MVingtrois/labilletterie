/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package souscontrollers;

import entite.Adresse;
import entite.Client;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sessionejb.InscriptionLocal;

/**
 *
 * @author cdi206
 */
public class SousControllerInscription implements SousControllerIt, Serializable {

    InscriptionLocal inscription = lookupInscriptionLocal();

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, HttpServlet Controller) {

        String page = "/index.html";

        if (request.getParameter("inscrire") != null) {
            Client clientNew = new Client();
            clientNew.setId(BigDecimal.valueOf(1));
            clientNew.setCivilite(request.getParameter("civilite"));
            clientNew.setNom(request.getParameter("nom"));
            clientNew.setPrenom(request.getParameter("prenom"));
            clientNew.setLogin(request.getParameter("login"));
            clientNew.setMotDePasse(request.getParameter("mdp"));
            try {
                clientNew.setNaissance(StringToDate(request.getParameter("naissance"), "dd/MM/yyyy"));
            } catch (ParseException ex) {
                System.out.println("Error parse StringToDate" + ex.getMessage());
            }
            clientNew.setMail(request.getParameter("mail"));

            System.out.println(clientNew.toString());
           
            Adresse adresse = new Adresse();
            adresse.setId(BigDecimal.valueOf(1));
            adresse.setNumeroRue(Integer.valueOf(request.getParameter("numRue")));
            adresse.setTypeRue(request.getParameter("typeRue"));
            adresse.setNomRue(request.getParameter("nomRue"));
            adresse.setCodePostal(request.getParameter("cp"));
            adresse.setVille(request.getParameter("ville"));
            
            System.out.println("Adresse :" + adresse.toString());
            
            inscription.Inscrire(clientNew, adresse);
            
            System.out.println("Enregistré !!!");

        } else {
            page = "/WEB-INF/Inscription.jsp";
            System.out.println("Direction index");
        }

        return page;
    }

    public static Date StringToDate(String stringDate, String pattern) throws ParseException {
        Date date = null;
        if (stringDate != null && !stringDate.isEmpty()) {
            SimpleDateFormat formatter = new SimpleDateFormat(pattern);
            date = formatter.parse(stringDate);
        }
        return date;
    }

    private InscriptionLocal lookupInscriptionLocal() {
        try {
            Context c = new InitialContext();
            return (InscriptionLocal) c.lookup("java:global/laBilletterieFrontOffice/laBilletterieFrontOffice-ejb/Inscription!sessionejb.InscriptionLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
