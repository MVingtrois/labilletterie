/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package souscontrollers;

import entite.Client;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionejb.ConnexionModifClientLocal;

/**
 *
 * @author cdi206
 */
public class SousControllerConnexion implements Serializable, SousControllerIt {
    ConnexionModifClientLocal connexion = lookupConnexionModifClientLocal();

   
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, HttpServlet Controller) {

        HttpSession session = request.getSession();
        String login = request.getParameter("login");
        String mdp = request.getParameter("mdp");
        String page = "/WEB-INF/Connexion.jsp";

        if (login != null && mdp != null) {

            Client client = connexion.connexion(login, mdp);
            System.out.println("Client----------------- " + client.toString());
            session.setAttribute("Client", client);
            System.out.println("après session Client----------------- " + client.toString());
            System.out.println("Session client = "+session.getAttribute("Client") );
            page = "/WEB-INF/Connecter.jsp";
        }
        
        

        return page;

    }

    private ConnexionModifClientLocal lookupConnexionModifClientLocal() {
        try {
            Context c = new InitialContext();
            return (ConnexionModifClientLocal) c.lookup("java:global/laBilletterieFrontOffice/laBilletterieFrontOffice-ejb/ConnexionModifClient!sessionejb.ConnexionModifClientLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    

}
