/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package souscontrollers;

import java.io.Serializable;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author cdi206
 */
public class SousControllerDeconnexion implements Serializable, SousControllerIt {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, HttpServlet Controller) {

        HttpSession session = request.getSession();
        String page = "/index.html";

        if (session.getAttribute("Client") != null) {
            session.setAttribute("Client", null);
            System.out.println("Client deconnecté");
            page = "/WEB-INF/Deconnecter.jsp";
        }

        return page;

    }

}
