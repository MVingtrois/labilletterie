/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package souscontrollers;

import entite.Client;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sessionejb.ConnexionModifClientLocal;
import static souscontrollers.SousControllerInscription.StringToDate;

/**
 *
 * @author cdi206
 */
public class SousControllerModifClient implements Serializable, SousControllerIt {
    ConnexionModifClientLocal modifClient = lookupConnexionModifClientLocal();
    

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response, HttpServlet Controller) {
        String page = "/index.html";

        HttpSession session = request.getSession();
        if (session.getAttribute("Client") != null) {
            Client clientSession = (Client) session.getAttribute("Client");
            request.setAttribute("nom", clientSession.getNom());
            request.setAttribute("prenom", clientSession.getPrenom());
            request.setAttribute("login", clientSession.getLogin());
            request.setAttribute("mdp", clientSession.getMotDePasse());
            request.setAttribute("mail", clientSession.getMail());
            request.setAttribute("civilite", clientSession.getCivilite());
            request.setAttribute("naissance", clientSession.getNaissance());
            System.out.println("------------------Client Modif--------------");
            page = "/WEB-INF/ModifClient.jsp";

            if (request.getParameter("modif") != null) {
                Client clientModif = new Client();
                clientModif.setId(clientSession.getId());
                clientModif.setCivilite(request.getParameter("civilite"));
                clientModif.setNom(request.getParameter("nom"));
                clientModif.setPrenom(request.getParameter("prenom"));
                clientModif.setLogin(request.getParameter("login"));
                clientModif.setMotDePasse(request.getParameter("mdp"));
                try {
                    clientModif.setNaissance(StringToDate(request.getParameter("naissance"), "dd/MM/yyyy"));
                } catch (ParseException ex) {
                    System.out.println("Error parse StringToDate" + ex.getMessage());
                }
                clientModif.setMail(request.getParameter("mail"));
                
                modifClient.modifClient(clientModif);
                
                session.setAttribute("Client", clientModif);
                
                page="/WEB-INF/Modifier.jsp";
            }
        }

        return page;
    }

    private ConnexionModifClientLocal lookupConnexionModifClientLocal() {
        try {
            Context c = new InitialContext();
            return (ConnexionModifClientLocal) c.lookup("java:global/laBilletterieFrontOffice/laBilletterieFrontOffice-ejb/ConnexionModifClient!sessionejb.ConnexionModifClientLocal");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

}
