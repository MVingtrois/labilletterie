package appliMain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 *
 * @author Eugène
 */
public class Creation {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("creationDBlaBilletteriePU");
        EntityManager em = emf.createEntityManager();


        EntityTransaction et = em.getTransaction();
        et.begin();
        et.commit();

        em.close();

    }

}
