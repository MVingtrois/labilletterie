package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Participant implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Nom", nullable = true, length = 255)
    private String nom;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @ManyToMany(mappedBy ="lesParticipants" )
    private Collection<Role> lesRroles;
    
    @ManyToMany
    private Collection<Evenement> lesEvenements;

    public Participant(Long id, String nom, boolean suppressionLogique, Collection<Role> lesRroles, Collection<Evenement> lesEvenements) {
        this.id = id;
        this.nom = nom;
        this.suppressionLogique = suppressionLogique;
        this.lesRroles = lesRroles;
        this.lesEvenements = lesEvenements;
    }    

    public Participant() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    } 

    public Collection<Role> getLesRroles() {
        return lesRroles;
    }

    public void setLesRroles(Collection<Role> lesRroles) {
        this.lesRroles = lesRroles;
    }

    public Collection<Evenement> getLesEvenements() {
        return lesEvenements;
    }

    public void setLesEvenements(Collection<Evenement> lesEvenements) {
        this.lesEvenements = lesEvenements;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Participant)) {
            return false;
        }
        Participant other = (Participant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Participant[ id=" + id + " ]";
    }
    
}
