package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Role implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Libelle", nullable = true, length = 255)
    private String libelle;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;
    
    @ManyToMany
    private Collection<Participant> lesParticipants;

    public Role(Long id, String libelle, boolean suppressionLogique, Collection<Participant> lesParticipants) {
        this.id = id;
        this.libelle = libelle;
        this.suppressionLogique = suppressionLogique;
        this.lesParticipants = lesParticipants;
    }

    public Role() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Participant> getLesParticipants() {
        return lesParticipants;
    }

    public void setLesParticipants(Collection<Participant> lesParticipants) {
        this.lesParticipants = lesParticipants;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Role)) {
            return false;
        }
        Role other = (Role) object;
        return (this.id != null || other.id == null) && (this.id == null || this.id.equals(other.id));
    }

    @Override
    public String toString() {
        return "entities.Role[ id=" + id + " ]";
    }
    
}
