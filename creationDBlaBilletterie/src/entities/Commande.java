package entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Commande implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "DateCommande", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private java.util.Date dateCommande;

    @Column(name = "NumeroCommande", nullable = true, length = 255)
    private String numeroCommande;

    @Column(name = "Statut", nullable = true, length = 255)
    private String statut;

    @Column(name = "Montant", nullable = true, precision = 19, scale = 0)
    private java.math.BigDecimal montant;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @ManyToOne
    private Client client;

    @ManyToOne
    private ModePaiment modePaiement;

    @ManyToOne
    private ModeLivraison modeLivraison;

    @OneToOne(mappedBy = "factureCommande")
    private Adresse adresseFacturation;
    
    @ManyToOne
    private Adresse adresseLivraison;

    @OneToMany(mappedBy = "commande")
    private Collection<Ticket> lesTickets;

    public Commande(Long id, Date dateCommande, String numeroCommande, String statut, BigDecimal montant, boolean suppressionLogique, Client client, ModePaiment modePaiement, ModeLivraison modeLivraison, Adresse adresseFacturation, Adresse adresseLivraison, Collection<Ticket> lesTickets) {
        this.id = id;
        this.dateCommande = dateCommande;
        this.numeroCommande = numeroCommande;
        this.statut = statut;
        this.montant = montant;
        this.suppressionLogique = suppressionLogique;
        this.client = client;
        this.modePaiement = modePaiement;
        this.modeLivraison = modeLivraison;
        this.adresseFacturation = adresseFacturation;
        this.adresseLivraison = adresseLivraison;
        this.lesTickets = lesTickets;
    }

    public Commande() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public String getNumeroCommande() {
        return numeroCommande;
    }

    public void setNumeroCommande(String numeroCommande) {
        this.numeroCommande = numeroCommande;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public BigDecimal getMontant() {
        return montant;
    }

    public void setMontant(BigDecimal montant) {
        this.montant = montant;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public ModePaiment getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(ModePaiment modePaiement) {
        this.modePaiement = modePaiement;
    }

    public ModeLivraison getModeLivraison() {
        return modeLivraison;
    }

    public void setModeLivraison(ModeLivraison modeLivraison) {
        this.modeLivraison = modeLivraison;
    }

    public Adresse getAdresseFacturation() {
        return adresseFacturation;
    }

    public void setAdresseFacturation(Adresse adresseFacturation) {
        this.adresseFacturation = adresseFacturation;
    }

    public Adresse getAdresseLivraison() {
        return adresseLivraison;
    }

    public void setAdresseLivraison(Adresse adresseLivraison) {
        this.adresseLivraison = adresseLivraison;
    }

    public Collection<Ticket> getLesTickets() {
        return lesTickets;
    }

    public void setLesTickets(Collection<Ticket> lesTickets) {
        this.lesTickets = lesTickets;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Commande[ id=" + id + " ]";
    }
    
}
