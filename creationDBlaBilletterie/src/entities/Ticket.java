package entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Ticket implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Numero", nullable = true, length = 255)
    private String numero;

    @Column(name = "ETicket", nullable = true)
    private boolean eTicket;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;
    
    @ManyToOne
    private Seance seance;
    
    @ManyToOne
    private Commande commande;

    @OneToOne
    private Place place;

    public Ticket() {
    }

    public Ticket(Long id, String numero, boolean eTicket, boolean suppressionLogique, Seance seance, Commande commande, Place place) {
        this.id = id;
        this.numero = numero;
        this.eTicket = eTicket;
        this.suppressionLogique = suppressionLogique;
        this.seance = seance;
        this.commande = commande;
        this.place = place;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean iseTicket() {
        return eTicket;
    }

    public void seteTicket(boolean eTicket) {
        this.eTicket = eTicket;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Seance getSeance() {
        return seance;
    }

    public void setSeance(Seance seance) {
        this.seance = seance;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ticket other = (Ticket) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
    
}
