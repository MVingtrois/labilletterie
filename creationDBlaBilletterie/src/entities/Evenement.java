package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Evenement implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Titre", nullable = true, length = 255)
    private String titre;

    @Column(name = "UrlAffiche", nullable = true, length = 255)
    private String urlAffiche;

    @Column(name = "Description", nullable = true, length = 255)
    private String Description;

    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;
    
    @ManyToOne
    private Genre genre;
    
    @ManyToOne
    private Theme theme;
    
    @ManyToOne
    private TVA tva;
    
    @OneToMany(mappedBy = "evenement")
    private Collection<Seance> listSeances;
    
    @ManyToMany(mappedBy = "lesEvenements")
    private Collection<Participant> lesParticipants;
    
    @ManyToMany(mappedBy = "lesEvenements")
    private Collection<Lieu> lesLieux;

    public Evenement(Long id, String titre, String urlAffiche, String Description, boolean suppressionLogique, Genre genre, Theme theme, TVA tva, Collection<Seance> listSeances, Collection<Participant> lesParticipants, Collection<Lieu> lesLieux) {
        this.id = id;
        this.titre = titre;
        this.urlAffiche = urlAffiche;
        this.Description = Description;
        this.suppressionLogique = suppressionLogique;
        this.genre = genre;
        this.theme = theme;
        this.tva = tva;
        this.listSeances = listSeances;
        this.lesParticipants = lesParticipants;
        this.lesLieux = lesLieux;
    }

    public Evenement() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUrlAffiche() {
        return urlAffiche;
    }

    public void setUrlAffiche(String urlAffiche) {
        this.urlAffiche = urlAffiche;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Theme getTheme() {
        return theme;
    }

    public void setTheme(Theme theme) {
        this.theme = theme;
    }

    public TVA getTva() {
        return tva;
    }

    public void setTva(TVA tva) {
        this.tva = tva;
    }

    public Collection<Seance> getListSeances() {
        return listSeances;
    }

    public void setListSeances(Collection<Seance> listSeances) {
        this.listSeances = listSeances;
    }

    public Collection<Participant> getLesParticipants() {
        return lesParticipants;
    }

    public void setLesParticipants(Collection<Participant> lesParticipants) {
        this.lesParticipants = lesParticipants;
    }

    public Collection<Lieu> getLesLieux() {
        return lesLieux;
    }

    public void setLesLieux(Collection<Lieu> lesLieux) {
        this.lesLieux = lesLieux;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evenement)) {
            return false;
        }
        Evenement other = (Evenement) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Evenement[ id=" + id + " ]";
    }
    
}
