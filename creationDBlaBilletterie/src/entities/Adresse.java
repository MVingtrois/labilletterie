package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Adresse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TypeRue", nullable = true, length = 255)
    private String typeRue;

    @Column(name = "NumeroRue", nullable = true)
    private int numeroRue;

    @Column(name = "NomRue", nullable = true, length = 255)
    private String nomRue;

    @Column(name = "Ville", nullable = true, length = 255)
    private String ville;

    @Column(name = "CodePostal", nullable = true, length = 255)
    private String codePostal;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;
    
    @ManyToMany
    private Collection<Client> lesClients;

    @ManyToMany
    private Collection<Lieu> lesLieux;

    @OneToMany(mappedBy = "adresseLivraison")
    private Collection<Commande> lesCommandes;
    
    @OneToOne
    private Commande factureCommande;

    public Adresse() {
    }

    public Adresse(Long id, String typeRue, int numeroRue, String nomRue, String ville, String codePostal, boolean suppressionLogique, Collection<Client> lesClients, Collection<Lieu> lesLieux, Collection<Commande> lesCommandes, Commande factureCommande) {
        this.id = id;
        this.typeRue = typeRue;
        this.numeroRue = numeroRue;
        this.nomRue = nomRue;
        this.ville = ville;
        this.codePostal = codePostal;
        this.suppressionLogique = suppressionLogique;
        this.lesClients = lesClients;
        this.lesLieux = lesLieux;
        this.lesCommandes = lesCommandes;
        this.factureCommande = factureCommande;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeRue() {
        return typeRue;
    }

    public void setTypeRue(String typeRue) {
        this.typeRue = typeRue;
    }

    public int getNumeroRue() {
        return numeroRue;
    }

    public void setNumeroRue(int numeroRue) {
        this.numeroRue = numeroRue;
    }

    public String getNomRue() {
        return nomRue;
    }

    public void setNomRue(String nomRue) {
        this.nomRue = nomRue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Client> getLesClients() {
        return lesClients;
    }

    public void setLesClients(Collection<Client> lesClients) {
        this.lesClients = lesClients;
    }

    public Collection<Lieu> getLesLieux() {
        return lesLieux;
    }

    public void setLesLieux(Collection<Lieu> lesLieux) {
        this.lesLieux = lesLieux;
    }

    public Collection<Commande> getLesCommandes() {
        return lesCommandes;
    }

    public void setLesCommandes(Collection<Commande> lesCommandes) {
        this.lesCommandes = lesCommandes;
    }

    public Commande getFactureCommande() {
        return factureCommande;
    }

    public void setFacture(Commande factureCommande) {
        this.factureCommande = factureCommande;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Adresse other = (Adresse) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

   
}
