package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class ModeLivraison implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Type", nullable = true, length = 255)
    private String type;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @OneToMany(mappedBy = "modeLivraison")
    private Collection<Commande> lesCommandes;

    public ModeLivraison(Long id, String type, boolean suppressionLogique, Collection<Commande> lesCommandes) {
        this.id = id;
        this.type = type;
        this.suppressionLogique = suppressionLogique;
        this.lesCommandes = lesCommandes;
    }

    public ModeLivraison() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Commande> getLesCommandes() {
        return lesCommandes;
    }

    public void setLesCommandes(Collection<Commande> lesCommandes) {
        this.lesCommandes = lesCommandes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModeLivraison)) {
            return false;
        }
        ModeLivraison other = (ModeLivraison) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.ModeLivraison[ id=" + id + " ]";
    }
    
}
