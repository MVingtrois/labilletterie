package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Zone implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Libelle", nullable = true, length = 255)
    private String libelle;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @OneToMany(mappedBy = "zone")
    private Collection<Place> lesPlaces;

    public Zone(Long id, String libelle, boolean suppressionLogique, Collection<Place> lesPlaces) {
        this.id = id;
        this.libelle = libelle;
        this.suppressionLogique = suppressionLogique;
        this.lesPlaces = lesPlaces;
    }

    public Zone() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Place> getLesPlaces() {
        return lesPlaces;
    }

    public void setLesPlaces(Collection<Place> lesPlaces) {
        this.lesPlaces = lesPlaces;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Zone)) {
            return false;
        }
        Zone other = (Zone) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Zone[ id=" + id + " ]";
    }
    
}
