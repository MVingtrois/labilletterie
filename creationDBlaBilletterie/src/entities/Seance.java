package entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Seance implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "Date", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    @Column(name = "Heure", nullable = true)
    private java.sql.Time heure;

    @Column(name = "Libelle", nullable = true, length = 255)
    private String libelle;

    @Column(name = "Duree", nullable = true, length = 255)
    private String duree;

    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @ManyToOne
    private Evenement evenement;

    @OneToMany(mappedBy = "seance")
    private Collection<Ticket> listTickets;

    @ManyToOne
    private Lieu lieu;

    public Seance() {
    }

    public Seance(Long id, Date date, Time heure, String libelle, String duree, boolean suppressionLogique, Evenement evenement, Collection<Ticket> listTickets, Lieu lieu) {
        this.id = id;
        this.date = date;
        this.heure = heure;
        this.libelle = libelle;
        this.duree = duree;
        this.suppressionLogique = suppressionLogique;
        this.evenement = evenement;
        this.listTickets = listTickets;
        this.lieu = lieu;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getHeure() {
        return heure;
    }

    public void setHeure(Time heure) {
        this.heure = heure;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getDuree() {
        return duree;
    }

    public void setDuree(String duree) {
        this.duree = duree;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Evenement getEvenement() {
        return evenement;
    }

    public void setEvenement(Evenement evenement) {
        this.evenement = evenement;
    }

    public Collection<Ticket> getListTickets() {
        return listTickets;
    }

    public void setListTickets(Collection<Ticket> listTickets) {
        this.listTickets = listTickets;
    }

    public Lieu getLieu() {
        return lieu;
    }

    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Seance other = (Seance) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
