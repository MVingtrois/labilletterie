package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Categorie implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Libelle", nullable = true, length = 255)
    private String libelle;

    @Column(name = "Prix", nullable = true, length = 10)
    private Float prix;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @OneToMany(mappedBy = "categorie")
    private Collection<Place> lesPlaces;

    @ManyToMany(mappedBy = "lesCategories")
    private Collection<Reduction> lesReductions;

    public Categorie(Long id, String libelle, Float prix, boolean suppressionLogique, Collection<Place> lesPlaces, Collection<Reduction> lesReductions) {
        this.id = id;
        this.libelle = libelle;
        this.prix = prix;
        this.suppressionLogique = suppressionLogique;
        this.lesPlaces = lesPlaces;
        this.lesReductions = lesReductions;
    }

    public Categorie() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getPrix() {
        return prix;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Place> getLesPlaces() {
        return lesPlaces;
    }

    public void setLesPlaces(Collection<Place> lesPlaces) {
        this.lesPlaces = lesPlaces;
    }

    public Collection<Reduction> getLesReductions() {
        return lesReductions;
    }

    public void setLesReductions(Collection<Reduction> lesReductions) {
        this.lesReductions = lesReductions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categorie)) {
            return false;
        }
        Categorie other = (Categorie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Categorie[ id=" + id + " ]";
    }
    
}
