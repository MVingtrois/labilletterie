package entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Place implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
     @Column(name = "Disponible", nullable = false)
    private boolean disponible;

    @Column(name = "Numero", nullable = true, length = 255)
    private String numero;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @OneToOne(mappedBy = "placeSuivi")
    private Place placePrecedante;

    @OneToOne(mappedBy = "placePrecede")
    private Place placeSuivante;
    
    @OneToOne
    private Place placeSuivi;
    
    @OneToOne
    private Place placePrecede;

    @OneToOne(mappedBy = "place")
    private Ticket ticket;

    @ManyToOne
    private Categorie categorie;

    @ManyToOne
    private Lieu lieu;

    @ManyToOne
    private Zone zone;

    public Place(Long id, boolean disponible, String numero, boolean suppressionLogique, Place placePrecedante, Place placeSuivante, Place placeSuivi, Place placePrecede, Ticket ticket, Categorie categorie, Lieu lieu, Zone zone) {
        this.id = id;
        this.disponible = disponible;
        this.numero = numero;
        this.suppressionLogique = suppressionLogique;
        this.placePrecedante = placePrecedante;
        this.placeSuivante = placeSuivante;
        this.placeSuivi = placeSuivi;
        this.placePrecede = placePrecede;
        this.ticket = ticket;
        this.categorie = categorie;
        this.lieu = lieu;
        this.zone = zone;
    }

    public Place() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Place getPlacePrecedante() {
        return placePrecedante;
    }

    public void setPlacePrecedante(Place placePrecedante) {
        this.placePrecedante = placePrecedante;
    }

    public Place getPlaceSuivante() {
        return placeSuivante;
    }

    public void setPlaceSuivante(Place placeSuivante) {
        this.placeSuivante = placeSuivante;
    }

    public Place getPlaceSuivi() {
        return placeSuivi;
    }

    public void setPlaceSuivi(Place placeSuivi) {
        this.placeSuivi = placeSuivi;
    }

    public Place getPlacePrecede() {
        return placePrecede;
    }

    public void setPlacePrecede(Place placePrecede) {
        this.placePrecede = placePrecede;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public Lieu getLieu() {
        return lieu;
    }

    public void setLieu(Lieu lieu) {
        this.lieu = lieu;
    }

    public Zone getZone() {
        return zone;
    }

    public void setZone(Zone zone) {
        this.zone = zone;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Place other = (Place) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    
}