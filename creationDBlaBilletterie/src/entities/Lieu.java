package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Lieu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "UrlPlan", nullable = true, length = 255)
    private String urlPlan;

    @Column(name = "Nom", nullable = true, length = 255)
    private String nom;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @ManyToMany(mappedBy="lesLieux")
    private Collection<Adresse> lesAdresses;
    
    @ManyToMany
    private Collection<Evenement> lesEvenements;
   
    @OneToMany(mappedBy = "lieu")
    private Collection<Place> lesPlaces;
    
    @OneToMany(mappedBy = "lieu")
    private Collection<Seance> lesSeances; 

    public Lieu(Long id, String urlPlan, String nom, boolean suppressionLogique, Collection<Adresse> lesAdresses, Collection<Evenement> lesEvenements, Collection<Place> lesPlaces, Collection<Seance> lesSeances) {
        this.id = id;
        this.urlPlan = urlPlan;
        this.nom = nom;
        this.suppressionLogique = suppressionLogique;
        this.lesAdresses = lesAdresses;
        this.lesEvenements = lesEvenements;
        this.lesPlaces = lesPlaces;
        this.lesSeances = lesSeances;
    }

    public Lieu() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrlPlan() {
        return urlPlan;
    }

    public void setUrlPlan(String urlPlan) {
        this.urlPlan = urlPlan;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Adresse> getLesAdresses() {
        return lesAdresses;
    }

    public void setLesAdresses(Collection<Adresse> lesAdresses) {
        this.lesAdresses = lesAdresses;
    }

    public Collection<Evenement> getLesEvenements() {
        return lesEvenements;
    }

    public void setLesEvenements(Collection<Evenement> lesEvenements) {
        this.lesEvenements = lesEvenements;
    }

    public Collection<Place> getLesPlaces() {
        return lesPlaces;
    }

    public void setLesPlaces(Collection<Place> lesPlaces) {
        this.lesPlaces = lesPlaces;
    }

    public Collection<Seance> getLesSeances() {
        return lesSeances;
    }

    public void setLesSeances(Collection<Seance> lesSeances) {
        this.lesSeances = lesSeances;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lieu)) {
            return false;
        }
        Lieu other = (Lieu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Lieu[ id=" + id + " ]";
    }
    
}
