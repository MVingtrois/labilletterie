package entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class TVA implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Taux", nullable = true, length = 10)
    private Float taux;

    @Column(name = "Date", nullable = true)
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date date;

    @Column(name = "Type", nullable = true, length = 255)
    private String type;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @OneToMany(mappedBy = "tva")
    private Collection<Evenement> lesEvenements;

    public TVA(Long id, Float taux, Date date, String type, boolean suppressionLogique, Collection<Evenement> lesEvenements) {
        this.id = id;
        this.taux = taux;
        this.date = date;
        this.type = type;
        this.suppressionLogique = suppressionLogique;
        this.lesEvenements = lesEvenements;
    }

    public TVA() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getTaux() {
        return taux;
    }

    public void setTaux(Float taux) {
        this.taux = taux;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Evenement> getLesEvenements() {
        return lesEvenements;
    }

    public void setListEvenements(Collection<Evenement> lesEvenements) {
        this.lesEvenements = lesEvenements;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TVA)) {
            return false;
        }
        TVA other = (TVA) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.TVA[ id=" + id + " ]";
    }
    
}
