package entities;

import java.io.Serializable;
import java.util.Collection;
import java.sql.Date;
import java.util.Objects;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Client implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    public enum Civilites{
        M,
        Mme
    }
    
    @Column(name = "civilite")
    @Enumerated(EnumType.STRING) 
    private Civilites civilite;
    
    @Column(name = "Nom", nullable = true, length = 255)
    private String nom;

    @Column(name = "Prenom", nullable = true, length = 255)
    private String prenom;

    @Column(name = "Naissance", nullable = true)
    private Date naissance;

    @Column(name = "MotDePasse", nullable = true, length = 255)
    private String motDePasse;

    @Column(name = "Login", nullable = true, length = 255)
    private String login;

    @Column(name = "Mail", nullable = true, length = 255)
    private String mail;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;
    
    @ManyToMany(mappedBy="lesClients")
    private Collection<Adresse> lesAdresses;

    @OneToMany(mappedBy = "client")
    private Collection<Commande> lesCommances;

    public Client() {
    }

    public Client(Civilites civilite, String nom, String prenom, Date naissance, String motDePasse, String login, String mail, boolean suppressionLogique, Collection<Adresse> lesAdresses, Collection<Commande> lesCommances) {
        this.civilite = civilite;
        this.nom = nom;
        this.prenom = prenom;
        this.naissance = naissance;
        this.motDePasse = motDePasse;
        this.login = login;
        this.mail = mail;
        this.suppressionLogique = suppressionLogique;
        this.lesAdresses = lesAdresses;
        this.lesCommances = lesCommances;
    }

   

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Civilites getCivilite() {
        return civilite;
    }

    public void setCivilite(Civilites civilite) {
        this.civilite = civilite;
    }
   
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getNaissance() {
        return naissance;
    }

    public void setNaissance(Date naissance) {
        this.naissance = naissance;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Adresse> getLesAdresses() {
        return lesAdresses;
    }

    public void setLesAdresses(Collection<Adresse> lesAdresses) {
        this.lesAdresses = lesAdresses;
    }

    public Collection<Commande> getLesCommances() {
        return lesCommances;
    }

    public void setLesCommances(Collection<Commande> lesCommances) {
        this.lesCommances = lesCommances;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    
    
}
