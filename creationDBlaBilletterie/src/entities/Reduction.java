package entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;

/**
 *
 * @author Eugène
 */
@Entity
public class Reduction implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "Libelle", nullable = true, length = 255)
    private String libelle;

    @Column(name = "Montant", nullable = true, length = 10)
    private Float montant;
    
    @Column(name = "SuppressionLogique", nullable = true)
    private boolean suppressionLogique;

    @ManyToMany
    private Collection<Categorie> lesCategories;

    public Reduction() {
    }

    public Reduction(Long id, String libelle, Float montant, boolean suppressionLogique, Collection<Categorie> lesCategories) {
        this.id = id;
        this.libelle = libelle;
        this.montant = montant;
        this.suppressionLogique = suppressionLogique;
        this.lesCategories = lesCategories;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Float getMontant() {
        return montant;
    }

    public void setMontant(Float montant) {
        this.montant = montant;
    }

    public boolean isSuppressionLogique() {
        return suppressionLogique;
    }

    public void setSuppressionLogique(boolean suppressionLogique) {
        this.suppressionLogique = suppressionLogique;
    }

    public Collection<Categorie> getLesCategories() {
        return lesCategories;
    }

    public void setLesCategories(Collection<Categorie> lesCategories) {
        this.lesCategories = lesCategories;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reduction)) {
            return false;
        }
        Reduction other = (Reduction) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Reduction[ id=" + id + " ]";
    }
    
}
